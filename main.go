package main

import "fmt"

//Menu func para exibir o menu
func Menu() int {
	fmt.Println("----------MENU----------")
	fmt.Println("| 0 - Quit             |")
	fmt.Println("| 1 - Add contact      |")
	fmt.Println("| 2 - Contact list     |")
	fmt.Println("| 3 - Search contact   |")
	fmt.Println("| 4 - Delete contact   |")
	fmt.Println("| 5 - Save agenda      |")
	fmt.Println("------------------------")

	op := -1

	for op < 0 || op > 5 {
		fmt.Scan(&op)
		fmt.Println("-------------------------------")

		if op < 0 || op > 5 {
			fmt.Println("Opção inválida!")
		}
	}

	return op
}

//MenuAgendas func do menu da agenda
func MenuAgendas() int {
	fmt.Println("----------MENU AGENDA----------")
	fmt.Println("| 0 - Quit                    |")
	fmt.Println("| 1 - New agenda              |")
	fmt.Println("| 2 - Load agenda             |")
	fmt.Println("| 3 - Delete agenda           |")
	fmt.Println("-------------------------------")

	op := -1

	for op < 0 || op > 3 {
		fmt.Scan(&op)
		fmt.Println("-------------------------------")

		if op < 0 || op > 3 {
			fmt.Println("Opção inválida!")
		}
	}

	return op
}

func main() {
	var agenda *Agenda

	err := ListAgendas()

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	for true {
		op := MenuAgendas()

		if op == 0 {
			fmt.Println("Até logo!")
			return
		} else if op == 1 {
			var err error
			agenda, err = NewAgenda()

			if err != nil {
				fmt.Println(err.Error())
				return
			}
			break
		} else if op == 2 {
			var err error
			agenda, err = OpenAgenda()

			if err != nil {
				fmt.Println(err.Error())
				return
			}
			break
		} else if op == 3 {
			err := DeleteAgenda()

			if err != nil {
				fmt.Println(err.Error())
				return
			}

		}
	}

	for true {
		op := Menu()
		if op == 0 {
			break
		} else if op == 1 {
			agenda.AddContact()
		} else if op == 2 {
			agenda.ListContacts()
		} else if op == 3 {
			agenda.SearchContact()
		} else if op == 4 {
			agenda.DeleteContact()
		} else if op == 5 {
			err := agenda.SaveAgenda()

			if err != nil {
				fmt.Println(err.Error())
				return
			}

		}
	}
}
