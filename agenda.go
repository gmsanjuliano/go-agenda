package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"strings"
)

//Agenda struct agendo do contato
type Agenda struct {
	Contacts []Contact `json:"contacts"`
	FileName string
}

//ListAgendas func listar agendas
func ListAgendas() error {
	fmt.Println("----------Agenda List----------")
	items, err := ioutil.ReadDir("agendas")

	if err != nil {
		return err
	}

	var count int = 0

	for _, item := range items {
		if !item.IsDir() && path.Ext(item.Name()) == ".json" {
			fmt.Println(strings.TrimSuffix(item.Name(), ".json"))
			count++
		}
	}

	if count == 0 {
		fmt.Println("Empty...")
		fmt.Println("-------------------------------")
		fmt.Println()
	} else {
		fmt.Println("-------------------------------")
		fmt.Println()
	}

	return nil
}

//NewAgenda func cria uma nova agenda
func NewAgenda() (*Agenda, error) {
	agenda := &Agenda{}

	fmt.Println("----------New Agenda----------")
	fmt.Print("Name: ")
	agenda.FileName = readString() + ".json"
	fileName := path.Join("agendas", agenda.FileName)
	_, err := os.Create(fileName)

	if err != nil {
		return nil, err
	}

	err = agenda.SaveAgenda()

	if err != nil {
		return nil, err
	}

	return agenda, nil
}

//OpenAgenda func abre o arquivo em agenda
func OpenAgenda() (*Agenda, error) {
	fmt.Println("----------Load Agenda----------")
	fmt.Print("Name: ")
	fileName := path.Join("agendas", readString()) + ".json"
	f, err := os.Open(fileName)
	defer f.Close()
	if err != nil {
		if os.IsNotExist(err) {
			fmt.Println("Agenda not found.")
		} else {
			fmt.Println(err.Error())
		}
		return nil, err
	}

	var agenda Agenda
	agenda.FileName = fileName
	err = json.NewDecoder(f).Decode(&agenda)

	if err != nil {
		return nil, err
	}

	return &agenda, nil
}

//DeleteAgenda func para deletar agendar
func DeleteAgenda() error {
	fmt.Print("Name: ")
	fileName := path.Join("agendas", readString()) + ".json"

	err := os.Remove(fileName)

	if err != nil {
		return err
	}
	fmt.Println("Agenda apagada com sucesso!")
	return nil
}

//SaveAgenda func salva a  agenda em arquivo
func (agenda *Agenda) SaveAgenda() error {
	fileName := path.Join("agendas", agenda.FileName)
	f, err := os.OpenFile(fileName, os.O_WRONLY, os.ModeExclusive)
	defer f.Close()
	if err != nil {
		return err
	}

	err = json.NewEncoder(f).Encode(agenda)
	fmt.Println("Agenda salva com sucesso!")
	fmt.Println()
	return err
}

//AddContact func para adicionar contatos
func (agenda *Agenda) AddContact() {
	contact := Contact{}

	fmt.Println("----------Add Contact----------")
	fmt.Print("Name: ")
	contact.Name = readString()
	fmt.Print("Phone: ")
	fmt.Scan(&contact.Phone)
	fmt.Println("Contato adicionado com sucesso!")
	fmt.Println("--------------------------------")

	agenda.Contacts = append(agenda.Contacts, contact)
}

//ListContacts func para listar os contatos
func (agenda *Agenda) ListContacts() {
	fmt.Println("----------Contact List----------")

	if len(agenda.Contacts) == 0 {
		fmt.Println("Empty...")
		fmt.Println("-------------------------------")
		return
	}

	for index, contact := range agenda.Contacts {
		fmt.Printf("Index: %v\n", index)
		fmt.Printf("Name: %v\n", contact.Name)
		fmt.Printf("Phone: %v\n", contact.Phone)
		fmt.Println("-------------------------------")
	}

}

//SearchContact func para buscar contato por nome
func (agenda *Agenda) SearchContact() {
	fmt.Println("----------Search Name----------")
	fmt.Print("Name: ")
	name := strings.ToLower(readString())
	fmt.Println("-------------------------------")

	for index, contact := range agenda.Contacts {
		if strings.Contains(strings.ToLower(contact.Name), name) {
			fmt.Printf("Index: %v\n", index)
			fmt.Printf("Name: %v\n", contact.Name)
			fmt.Printf("Phone: %v\n", contact.Phone)
			fmt.Println("-------------------------------")
		}
	}
}

//DeleteContact funca para deletar contato
func (agenda *Agenda) DeleteContact() {
	fmt.Println("----------Remove Contact-----------")
	fmt.Print("Index: ")
	var index int
	fmt.Scan(&index)

	if index >= 0 && index < len(agenda.Contacts) {
		agenda.Contacts = append(agenda.Contacts[:index], agenda.Contacts[index+1:]...)
		fmt.Println("Apagado com sucesso!")
	} else {
		fmt.Println("Índice não encontrado!")
	}
	fmt.Println("----------------------------")
}
