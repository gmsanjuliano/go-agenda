package main

//Contact struct de um contato
type Contact struct {
	Name  string `json:"name"`
	Phone string `json:"phone"`
}
